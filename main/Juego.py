import pickle


class Monstruo():
    def __init__(self):
        self.vida = 100
        self.elementos = []


class Agua:
    def __init__(self):
        self.ataque = "Fuego"
        self.defensa = "Tierra"

    def __repr__(self):
        return "Agua"


class Aire:
    def __init__(self):
        self.ataque = "Agua"
        self.defensa = "Fuego"

    def __repr__(self):
        return "Aire"


class Fuego:
    def __init__(self):
        self.ataque = "Tierra"
        self.defensa = "Agua"

    def __repr__(self):
        return "Fuego"


class Tierra:
    def __init__(self):
        self.ataque = "Aire"
        self.defensa = "Aire"

    def __repr__(self):
        return "Tierra"


class Menu:
    def __init__(self):
        self.elementos = [Agua(), Tierra(), Fuego(), Aire()]

    def iniciar_juego(self):
        print("Bienvenide a la lucha de monstruos!")
        opcion = int(input("Elija la opcion deseada:\n1. Nueva partida\n2. Cargar partida\n3. Salir"))
        if opcion == 1:
            Juego().jugar()
        elif opcion == 2:
            print("Cargar")
        elif opcion == 3:
            exit()

    def pedir_nombre(self, numero_jugador):
        mensaje = "Jugador " + str(numero_jugador) + " ingrese su nombre"
        return str(input(mensaje))

    def pedir_elementos(self):
        mensaje_elemento1 = "Elija su primer elemento: \n1. Agua\n2. Tierra\n3. Fuego\n4. Aire"
        opcion_elemento1 = int(input(mensaje_elemento1)) - 1
        mensaje_elemento2 = "Elija su segundo elemento: \n1. Agua\n2. Tierra\n3. Fuego\n4. Aire"
        opcion_elemento2 = int(input(mensaje_elemento2)) - 1

        return [self.elementos[opcion_elemento1], self.elementos[opcion_elemento2]]

    def pedir_elemento_ataque(self, elementos):
        mensaje = "Elija su elemento de ataque:\n1." + elementos[0].__repr__() + "\n2." + elementos[1].__repr__()
        return elementos[int(input(mensaje)) - 1]

    def pedir_tipo_ataque(self):
        mensaje = "Elija su tipo de ataque:\n1. Simple\n2. Especial"
        opcion = int(input(mensaje))
        if opcion == 1:
            return 10
        elif opcion == 2:
            return 15


class Jugador():

    def __init__(self):
        self.monstruo = Monstruo()
        self.nombre = ""

    def crear_jugador(self, numero):
        self.nombre = Menu().pedir_nombre(numero)
        self.monstruo.elementos = Menu().pedir_elementos()

    def atacar(self, jugador_atacado):
        jugador_atacado.monstruo.vida -= Juego().calculo_daño(Menu().pedir_tipo_ataque(),
                                                              Menu().pedir_elemento_ataque(self.monstruo.elementos),
                                                              jugador_atacado.monstruo)


class Juego:

    def __init__(self):
        self.jugador0 = Jugador()
        self.jugador1 = Jugador()

    def jugar(self):
        self.jugador0.crear_jugador(1)
        self.jugador0.crear_jugador(2)
        while self.jugador0.monstruo.vida > 0 and self.jugador1.monstruo.vida > 0:
            self.jugador0.atacar(self.jugador1)
            self.jugador1.atacar(self.jugador1)
        else:
            print("Juego finalizado")

    #             abrir menu otra vez

    def calculo_daño(self, daño, elemento_atacante, monstruo_atacado):
        c = 0
        d = 0
        if elemento_atacante.ataque == monstruo_atacado.elementos[0].__repr__():
            c = daño * 0.2
        elif elemento_atacante.ataque == monstruo_atacado.elementos[1].__repr__():
            c = daño * 0.2
        elif monstruo_atacado.elementos[0].defensa == elemento_atacante.__repr__():
            d = daño * 0.2
        elif monstruo_atacado.elementos[1].defensa == elemento_atacante.__repr__():
            d = daño * 0.2
        return daño + c - d

    def calculo_daño2(self, daño, elemento_atacante, monstruo_atacado):
        fortaleza, debilidad = 0, 0
        if elemento_atacante.ataque == monstruo_atacado.elementos[0].__repr__() or elemento_atacante.ataque == \
                monstruo_atacado.elementos[1].__repr__():
            fortaleza = daño * 0.2
        elif monstruo_atacado.elementos[0].__repr__() == elemento_atacante or monstruo_atacado.elementos[
            1].__repr__() == elemento_atacante:
            debilidad = daño * 0.2
        return daño + fortaleza - debilidad


Menu().iniciar_juego()
